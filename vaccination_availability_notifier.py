import sys
import getopt
from crontab import CronTab
import os
import getpass

job_script_path = os.path.dirname(os.path.abspath(__file__))
os.chmod(os.path.join(job_script_path,'job_def.py'), 0o775)
def main(argv):
    opts, args = getopt.getopt(argv, "")
    age = args[0]
    pin_code = args[1]
#    email_id = args[2]
    command='python3 {}/job_def.py {} {} >{}/logs 2>&1' .format(job_script_path, age , pin_code, job_script_path)
    cronjob = CronTab(user=getpass.getuser())
    job = cronjob.new(command)
    job.hour.every(1)
    cronjob.write()

if __name__ == '__main__':
    main(sys.argv[1:])
