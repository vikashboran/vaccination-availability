#from crontab import CronTab
import sys
import json
import getopt
import requests
from utils import *
from datetime import date, timedelta, datetime

opts, args = getopt.getopt(sys.argv, "")
age = args[1]
pin_code = args[2]
#email_id = args[3]
cur_date = date.today().strftime("%d-%m-%Y")
next_date = (datetime.now() + timedelta(days=1)).strftime("%d-%m-%Y")
result = []

url1 = 'https://cdn-api.co-vin.in/api/v2/appointment/sessions/public/findByPin?pincode={}&date={}'.format(pin_code, cur_date)
url2 = 'https://cdn-api.co-vin.in/api/v2/appointment/sessions/public/findByPin?pincode={}&date={}'.format(pin_code, next_date)

headers = {"user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36"}

todays_avail = requests.get(url1, headers=headers).json()

next_date_avail = requests.get(url2, headers=headers).json()

print(next_date_avail)
get_required_res(age, todays_avail, result)
print(result)
get_required_res(age, next_date_avail, result)
print(result)
if len(result) > 0:
    send_mail(result, pin_code)
    print("mail has been sent")

else:
    print("mail could not be sent")
