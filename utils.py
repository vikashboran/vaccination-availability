from cred import *
import smtplib
from datetime import datetime
import os
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

def get_required_res(age, avail_response, result):
    sessions = avail_response['sessions']
    for sess in sessions:
        if (int(age)>=sess['min_age_limit']) and (sess['available_capacity']>0):
            result.append(sess)

def send_mail(responses, pin_code):

    server = smtplib.SMTP('smtp.gmail.com:587')
    print("connected")
    server.ehlo()
    server.starttls()
    print("just before login")
    server.login(email_id, pwd)
    message_body =  get_message(responses)
   
    message = MIMEMultipart("alternative")
    message["Subject"] = "[Act Now!] Vaccination is available at your location"
    message["From"] = email_id 
    message["To"] = email_id
    # Write the plain text part
    text = f"""\ {str(message_body)}"""
    part1 = MIMEText(text, "plain")
    message.attach(part1)

    print("just before sending the mail")
    server.sendmail(email_id, email_id, message.as_string())
    server.quit()

def get_message(responses):
    message = "Vaccination availablity details at requested location \n"
    for i, resp in enumerate(responses):
        message += "{}.\nVaccination Point: {} \nDistrict: {}\nAvailable Capacity: {} \nAvailable Slots: {}\nCharges:{}\n\n\n".format(i+1,resp['name'], resp['district_name'], resp['available_capacity'], resp['slots'], resp['fee'])
    return message
