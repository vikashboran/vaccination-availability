# README #
- Well, if you have a machine with internet connection then you don't have to poll co-WIN websites/apps  
  to check the vaccination slot availability in your area.
- This project allow us to get the vaccination slot availability notification automatically when a slot for the specified area and age is available at your location. 
  It search for the avalablity on current date as well as on next date, if it gets one then sends a mail to the user specified email id.

### How do I use it? ###
It's super easy, just clone the repo and then do the following:

- You need to edit cred.py to provide *email id* and *password* where you would like to receive the notification.
- Excute the below command.
```
python3 vaccination_availability_notifier.py <age> <pin_code>
```

However you may need to install required packages if not have already installed in your machine.
